/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dataStructures.tree;

import java.util.List;

/**
 *
 * @author vivify
 */
public class Tree extends NodeSearch {

    private Node<Node> root;
    private Node target;

    public Tree(Node root) {
        this.root = new Node(null, root);
        addRecursive(this.root);
    }

    private void addRecursive(Node<Node> current) {
        // Fetch current Node children
        List<Node> nodeList = null;

        if (null != nodeList) {
            for (Node Node : jnodeList) {
                Node<Node> node = current.addChild(jNode);
                addRecursive(node);
            }
        }
    }

    @Override
    public Node executeSearch() {
        if (null == root) {
            return null;
        }
        searchRecursive(root);
        return target;
    }

    @Override
    protected void searchRecursive(Node<Node> currentNode) {
        if (searchStrategy.execute(currentNode.getValue())) {
            this.target = currentNode.getValue();
        }

        for (Node<Node> child : currentNode.getChildren()) {
            searchRecursive(child);
        }
    }

}
