/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dataStructures.tree;

/**
 *
 * @author vivify
 */
public abstract class NodeSearch {

    protected INodeSearchStrategy searchStrategy;

    public void changeSearchStrategy(INodeSearchStrategy searchStrategy) {
        this.searchStrategy = searchStrategy;
    }

    protected abstract Node executeSearch();

    protected abstract void searchRecursive(Node<Node> currentJNode);
}
